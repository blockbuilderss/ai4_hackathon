import RPi.GPIO as GPIO
import time
from PushingBox import PushingBox

pbox=PushingBox()
GPIO.setmode(GPIO.BCM)

GPIO.setup(2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(3, GPIO.OUT)
GPIO.output(3,False)
while True:
    input_state = GPIO.input(2)
    if input_state == False:
	GPIO.output(3,True)
        print('Button Pressed')
	pbox.push('vC7CA838ECDD31DD',room='101',name='Vamsi',id='CMJPP9640Q')
        time.sleep(0.2)
    else:
	GPIO.output(3,False)
