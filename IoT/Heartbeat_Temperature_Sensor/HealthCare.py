from pulsesensor import Pulsesensor
import time
import spidev
from pymongo import MongoClient

client = MongoClient('mongodb://healthcare:health123@ds151970.mlab.com:51970/healthcare')
db = client.healthcare


p = Pulsesensor()
p.startAsyncBPM()

spi1 = spidev.SpiDev()
spi1.open(0, 0)

def getReading(channel):
    # First pull the raw data from the chip
    rawData = spi1.xfer([1, (8 + channel) << 4, 0])
    # Process the raw data into something we understand.
    processedData = ((rawData[1] & 3) << 8) + rawData[2]
    return processedData


def convertVoltage(bitValue, decimalPlaces=2):
    voltage = (bitValue * 3.3) / float(1023)
    voltage = round(voltage, decimalPlaces)
    return voltage


def convertTemp(bitValue, decimalPlaces=2):
    # Converts to degrees Celsius
    temperature = ((bitValue * 330) / float(1023))
    # 3300 mV / (10 mV/deg C) = 330
    temperature = round(temperature, decimalPlaces)
    return temperature


try:
    while True:
        bpm = p.BPM
	tempData = getReading(1)
	tempVoltage = convertVoltage(tempData)
    	temperature = convertTemp(tempData)
        print ("Temp = {}".format(temperature))
        if bpm > 0:
            print("BPM: %d" % bpm)
        else:
            print("No Heartbeat found")
	bpm1 = str(bpm)
	temp1 = str(temperature)
	db.patients.update({'patientId': "CMJPP9430Q"}, {'$set':{'pulseRate': bpm1 , 'temperature': temp1}});
        time.sleep(2)
except:
    p.stopAsyncBPM()
