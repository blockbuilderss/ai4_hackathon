# AI4_Hackathon - Block Builders

## Holistic Intelligent Healthcare Using AI, IoT and Blockchain 

Sustainablility in Healthcare

How to adapt and leverage the power of Intelligent systems in healthcare
to achieve sustainability?

Holistic Intelligent Healthcare

The aim is to promote and assist the healthcare institutions, adapt to a holistic, intelligent and technology-driven healthcare system, which uses smart technologies like AI, IoT, BlockChain, to collect, process, generate reports, improving the precision in diagnosis and operational efficiency.

A digitized healthcare ecosystem will help the patients get high precision diagnosis using IoT, continually receiving alerts for any aberration when under monitoring using AI also helping them with digital records of their reports made available on the trusted centralized platform using BlockChain. The automated process along with Insurance company in the transaction and sharing of medical records will minimize any kind of fraudulent activity with respective to the health insurance amounts.

--------------------
Video Link
--------------------
https://www.youtube.com/watch?v=Gr3Qwx3s928

---------------------------
Machine Learning Data sets
---------------------------
Data Set - https://mimic.physionet.org/gettingstarted/demo/ 

MIMIC Code Base - https://github.com/MIT-LCP/mimic-code/

---------------------------
Website Link
---------------------------
https://health-care-iot.herokuapp.com/

---------------------------
Repository Link
---------------------------
https://bitbucket.org/blockbuilderss/ai4_hackathon/src/master/

---------------------------
Instruction for running
---------------------------
For instructions please go to individual folder and find the instructions.txt file
