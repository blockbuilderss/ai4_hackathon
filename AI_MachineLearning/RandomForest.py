import pandas as pd
import numpy as np
import xgboost as xgb
import sklearn.preprocessing as skpp
from sklearn.grid_search import RandomizedSearchCV
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import make_scorer
from sklearn.cross_validation import train_test_split
import datetime
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier

#Read CSV Data - CSV Data is retrieved from SQL Commmand
filename = 'vitals.csv'
names = ['heartrate', 'tempc', 'glucose', 'warning']
data = pd.read_csv(filename, names=names)

param_grid = {
              "n_estimators": [50,100,250,500],
             }
X=data.drop(['heartrate', 'tempc', 'glucose', 'warning'], 1) 
y=data[warning] # Vital Warning
forest = RandomForestClassifier( random_state=42)
rand_for = RandomizedSearchCV(forest, param_grid, scoring = 'roc_auc', n_iter=20, random_state=42)
_ = rand_for.fit(X,y)

print(rand_for.best_score_) # Finding out the score of Random Forest Algorithm

importances = rand_for.best_estimator_.feature_importances_
indices = np.argsort(importances)[::-1]
for f in range(X.shape[1]):
    print "{}) {} {}".format(f, X.columns[indices[f]], importances[indices[f]]) # Finding out the Importance of Vitals