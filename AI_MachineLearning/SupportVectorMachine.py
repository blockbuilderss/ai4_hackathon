import pandas as pd
import numpy as np
import xgboost as xgb
import sklearn.preprocessing as skpp
from sklearn.grid_search import RandomizedSearchCV
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import roc_auc_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import make_scorer
from sklearn.cross_validation import train_test_split
import datetime
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC

#Read CSV Data - CSV Data is retrieved from SQL Commmand
filename = 'vitals.csv'
names = ['heartrate', 'tempc', 'glucose', 'warning']
data = pd.read_csv(filename, names=names)

sv_clf = SVC(random_state=42)

param_grid = {'C': [ 0.0001, 0.001, 0.01, 1, 100, 1000, 2000, 2500, 3000], 
              'gamma': [ 0.01, 0.001, 0.002, 0.003, 0.004, 0.005, 0.0001], 
              'kernel': ['rbf']
              }
X=data.drop(['heartrate', 'tempc', 'glucose', 'warning'], 1) # Input - Vitals - Heartrate , Temprature , Glucose
y=data[warning] # Target - Vital Warning

svm = GridSearchCV(sv_clf, param_grid, cv=10, scoring="roc_auc")
svm.fit(X,y)

print(svm.best_score_) # Finding out the score of Support Vector Machine

importances = svm.best_estimator_.feature_importances_
indices = np.argsort(importances)[::-1]
for f in range(X.shape[1]):
    print "{}) {} {}".format(f, X.columns[indices[f]], importances[indices[f]]) # Finding out the Importance of Vitals