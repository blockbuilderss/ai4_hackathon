const express       = require("express");
const app           = express();
const bodyParser    = require("body-parser");
const path          = require("path");
const mongoose      = require("mongoose");
const port          = process.env.PORT || 3000;
const userApi       = require("./server/routes/userApi");
const dailyReportApi = require("./server/routes/dailyReportApi")
var cors            = require('cors');
const patientApi    = require('./server/routes/patientApi');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://healthcare:health123@ds151970.mlab.com:51970/healthcare', function(err) {
  if(err){
      console.log(err);
  }else{
      console.log("Connected to mongo db");
  }
});
app.use(cors());
app.use(express.static(path.join(__dirname,'dist')));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended:true}));

app.use('/user',userApi);
app.use('/dailyReport',dailyReportApi);
app.use('/patient',patientApi);

app.get('*',(req,res) => {
    res.sendFile(path.join(__dirname,'dist/index.html'));
});

app.listen(port,function(){
    console.log("server is running on localhost : "+port);
})