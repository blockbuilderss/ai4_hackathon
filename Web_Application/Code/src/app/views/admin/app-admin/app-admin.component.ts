import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions, Jsonp } from '@angular/http';
import 'rxjs/add/operator/toPromise'; 

@Component({
  selector: 'app-admin',
  templateUrl: './app-admin.component.html',
  styleUrls: ['./app-admin.component.css']
})
export class AppAdminComponent implements OnInit {
 
  patientData: any;

  constructor(private http: Http){
  }

  ngOnInit(){
    this.getPatientById();
  }
  
  getPatientById(){
    var url = '/patient/getAllPatients';
    let headers = new Headers({ 'Content-Type' : 'application/json' });
    let options = new RequestOptions({ headers : headers });
    var value = {
      patientId: 1
    }
    return this.http.get(url)
    .toPromise()
    .then((res)=>{
      res = JSON.parse(res['_body']);
      this.patientData = res['data'];
    })
    .catch((err)=>{
      console.log(err);
    })
  }

}

