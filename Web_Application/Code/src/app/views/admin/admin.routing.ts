import { Routes } from '@angular/router';

import { AppAdminComponent } from './app-admin/app-admin.component';

export const AdminRoutes: Routes = [
  {
    path: '',
    component: AppAdminComponent,
  }
];