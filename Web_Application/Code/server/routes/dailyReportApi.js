const express = require('express');
const router = express.Router();
const path = require("path");

var DailyReport = require("../models/dailyReport");

router.post('/updateDailyReport', function(req, res){
    var dailyReport = DailyReport();
    dailyReport.userId = req.body.userId;
    dailyReport.pulseRate = req.body.pulseRate;
    dailyReport.reportDate = req.body.reportDate;
    dailyReport.bloodPressure = req.body.bloodPressure;
    dailyReport.temperature = req.body.temperature;
    dailyReport.medicine1 = req.body.medicine1;
    dailyReport.medicine2 = req.body.medicine2;
    dailyReport.weight = req.body.weight;

        dailyReport.save(function(err, savedDailyReport){
            if(err){
                var message = 'Daily Report not updated!';
                if (err.errors) {
                }
                obj = {
                    success: false,
                    message: message,
                    status: 500
                }
                return res.status(500).send(obj);
            }else{
                    obj = {
                        success: true,
                        message: 'Daily Report Updated successfully',
                        status: 200,
                        data: savedDailyReport

                    }
                    return res.status(200).send(obj);
            }
        })
    
});

router.post('/getDailyReportsById', function(req, res){
    var userId = req.body.userId;

    DailyReport.find({ userId: userId}, function(err, dailyReports){
        if(err){
            console.log(err);
            obj = {
                success: false,
                message: 'Daily Report api failed', 
                status: 500
            }
            return res.status(500).send(obj);
        }else{
            obj = {
                success: true,
                message: 'Getting daily report list succesfully!',
                data: dailyReports,
                status: 200
            }
            return res.status(200).send(obj);
        }
    });
});
module.exports = router;