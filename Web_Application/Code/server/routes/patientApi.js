const express = require('express');
const router = express.Router();
const path = require("path");

var Patient = require("../models/patient");

router.post('/signup', function(req, res){
    var patient = Patient();
    patient.patientId = req.body.patientId;
    patient.patientName = req.body.patientName;
    patient.pulseRate = req.body.pulseRate;
    patient.temperature = req.body.temperature;
    patient.date = new Date();

        patient.save(function(err, savedPatient){
            if(err){
                var message = 'Patient already registered!';
                if (err.errors) {
                }
                obj = {
                    success: false,
                    message: message,
                    status: 500
                }
                return res.status(500).send(obj);
            }else{
                    obj = {
                        success: true,
                        message: 'Registered successfully, Please check your email to activate your account!',
                        status: 200
                    }
                    return res.status(200).send(obj);
            }
        })
    
});
router.get('/getAllPatients', function(req, res){
    Patient.find(function(err, patients){
        if(err){
            console.log(err);
            obj = {
                success: false,
                message: 'Patient api failed', 
                status: 500
            }
            return res.status(500).send(obj);
        }else{
            obj = {
                success: true,
                message: 'Getting patient list succesfully!',
                data: patients,
                status: 200
            }
            return res.status(200).send(obj);
        }
    });
});

router.post('/getPatientById', function(req, res){
    var patientId = req.body.patientId;

    Patient.find({ patientId: patientId},null, {sort: {date: -1}}, function(err, patients){
        if(err){
            console.log(err);
            obj = {
                success: false,
                message: 'Patient api failed', 
                status: 500
            }
            return res.status(500).send(obj);
        }else{
            obj = {
                success: true,
                message: 'Getting patient details succesfully!',
                data: patients[0],
                status: 200
            }
            return res.status(200).send(obj);
        }
    });
});

module.exports = router;