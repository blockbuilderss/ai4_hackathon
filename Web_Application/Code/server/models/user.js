var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var candidateSchema = new Schema({
    userId:String,
    name:String,
    phoneNo:String,
    age:String,
    gender:String,
    height:String,
    weight:String,
    pulseRate:String,
    temperature:String,
    bloodPressure:String
});

module.exports = mongoose.model('candidate', candidateSchema);