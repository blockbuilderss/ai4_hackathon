var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var patientSchema = new Schema({
    patientId:String,
    patientName:String,
    pulseRate:String,
    temperature:String,
    date:{type:Date}
});

module.exports = mongoose.model('patient', patientSchema);